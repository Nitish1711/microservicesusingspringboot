package com.springBoot.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springBoot.entity.PersonalInformation;
import com.springBoot.repository.PersonalInfoRepository;
import com.springBoot.service.PersonalInfoService;

@Service
public class PersonalInfoServiceImpl implements PersonalInfoService {
	
	@Autowired
	private PersonalInfoRepository personalInfoRepository;


	@Override
	public PersonalInformation saveEmployeePersonalInfo(PersonalInformation personalInfo) {
	PersonalInformation personalInformtn=personalInfoRepository.save(personalInfo);
	return personalInformtn;
		
	}

	@Override
	public PersonalInformation getEmployeePersonalInfo(int id) {
	  Optional<PersonalInformation> perosnalInfo=personalInfoRepository.findById(id);			
		return perosnalInfo.get();
	}

	@Override
	public void updateEmployeePersonalInfo(PersonalInformation personalInfo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean removeEmployeePersonalInfo(int id) {
		personalInfoRepository.deleteById(id);			
		return true;
	}

	@Override
	public PersonalInformation getEmployeePersonalInfoFromCode(String empCode) {
		  PersonalInformation perosnalInfo=personalInfoRepository.findByEmpCode(empCode);			
		  System.out.println("result is "+perosnalInfo);
			return perosnalInfo;
	}



}
