package com.springBoot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springBoot.entity.PersonalInformation;
import com.springBoot.service.PersonalInfoService;

@RestController
@Component
public class PersonalnformationController {

	
	@Autowired
	private PersonalInfoService personalInfoService;

	
	@PostMapping(path="/personalInformation",consumes = "application/json", produces = "application/json")
	
	public ResponseEntity<PersonalInformation> addPersonalInfo(@RequestBody PersonalInformation employeeRequest) {
		System.out.println("add personal info..");
		PersonalInformation isSaved = personalInfoService.saveEmployeePersonalInfo(employeeRequest);
	    return new ResponseEntity<PersonalInformation>(isSaved, HttpStatus.OK);
	}
	
	@GetMapping("/personalInformation/{id}")
	public PersonalInformation getEmployeePersonalInfo(@PathVariable int id) {
		System.out.println("getting personal info");
	  return personalInfoService.getEmployeePersonalInfo(id);
	}
	
	@GetMapping("/personalInformation/code/{empCode}")
	public PersonalInformation getEmployeePersonalInfoFromCode(@PathVariable String empCode) {
		System.out.println("getting personal info from code");
	  return personalInfoService.getEmployeePersonalInfoFromCode(empCode);
	}

	
	@PutMapping(path="/personalInformation",consumes = "application/json", produces = "application/json")
	
	public String editPersonalInfo(@RequestBody PersonalInformation employeeRequest) {
		System.out.println("editing personal info..");
		String result = "faliure";
		PersonalInformation isSaved = personalInfoService.saveEmployeePersonalInfo(employeeRequest);
 		return result;
	}

	@DeleteMapping("/personalInformation/{id}")
	public boolean deleteEmployeePersonalInfo(@PathVariable int id) {
		System.out.println("deleting personal info");
	   personalInfoService.removeEmployeePersonalInfo(id);
	   return true;
	}


}
