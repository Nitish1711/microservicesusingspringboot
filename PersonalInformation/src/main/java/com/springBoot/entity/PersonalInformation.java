/**
 * 
 */
package com.springBoot.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Dell
 *
 */

@Entity
@NoArgsConstructor
public class PersonalInformation {
	
	
	@Id
	@GeneratedValue
	private Integer id;
	
	private String empName;	
	
	private String age;
	
	private String empCode;
	
	private String designation;

	@Override
	public int hashCode() {
		return Objects.hash(age, designation, empCode, empName, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonalInformation other = (PersonalInformation) obj;
		return Objects.equals(age, other.age) && Objects.equals(designation, other.designation)
				&& Objects.equals(empCode, other.empCode) && Objects.equals(empName, other.empName)
				&& Objects.equals(id, other.id);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
	


}
