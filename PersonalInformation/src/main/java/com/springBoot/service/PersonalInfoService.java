package com.springBoot.service;

import com.springBoot.entity.PersonalInformation;


public interface PersonalInfoService {
	
	

	public PersonalInformation saveEmployeePersonalInfo(PersonalInformation personalInfo);
	
	public PersonalInformation getEmployeePersonalInfo(int id) ;
	
	public void updateEmployeePersonalInfo(PersonalInformation personalInfo);

	public boolean removeEmployeePersonalInfo(int id);

	public PersonalInformation getEmployeePersonalInfoFromCode(String empCode);


}
