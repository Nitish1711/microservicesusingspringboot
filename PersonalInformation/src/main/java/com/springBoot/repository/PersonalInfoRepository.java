package com.springBoot.repository;

import org.springframework.stereotype.Repository;

import com.springBoot.entity.PersonalInformation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface PersonalInfoRepository extends JpaRepository<PersonalInformation,Integer> {
	
    @Query("SELECT t FROM PersonalInformation t WHERE t.empCode =:empCode")
    public PersonalInformation findByEmpCode(@Param("empCode") String empCode);


}
