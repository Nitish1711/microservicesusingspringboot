package com.springBoot.serviceImpl;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.springBoot.model.EmployeeInfoRequest;
import com.springBoot.model.LeaveInformation;
import com.springBoot.model.PersonalInformation;
import com.springBoot.service.EmployeeGatewayService;

@Service
public class EmployeeGatewayServiceImpl implements EmployeeGatewayService {
	
	
	public RestTemplate restTemplate;
	
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	public boolean saveEmployeeDetails(EmployeeInfoRequest employee) {
		
		CompletableFuture<PersonalInformation> piFuture =CompletableFuture.supplyAsync(()->invokePersonalInformationService(employee));
		CompletableFuture<LeaveInformation> liFuture =CompletableFuture.supplyAsync(()->invokeLeaveInformationService(employee));
		CompletableFuture.allOf(piFuture,liFuture).join();
		try {
			System.out.println(piFuture.get());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		return true;
	}

	private PersonalInformation invokePersonalInformationService(EmployeeInfoRequest employee) {
		System.out.println("inside perosnal info");

		String uri="http://localhost:8080/personalInformation";
		
		//PersonalInformation personalInfo=populatePersonalInfo(employee);
		PersonalInformation personalInfo=restTemplate.postForObject(uri, populatePersonalInfo(employee), PersonalInformation.class);
		return personalInfo;
	}
	
	private LeaveInformation invokeLeaveInformationService(EmployeeInfoRequest employee) {
		System.out.println("inside leave info");
		String uri="http://localhost:8081/leaveInformation";
		LeaveInformation leaveInformtn=restTemplate.postForObject(uri, populateLeaveInfo(employee), LeaveInformation.class);
		return leaveInformtn;
	}


	private LeaveInformation populateLeaveInfo(EmployeeInfoRequest employee) {
		LeaveInformation leaveinfo=new LeaveInformation();
		leaveinfo.setAnnualLeaves(employee.getAnnualLeaves());
		leaveinfo.setCasualLeaves(employee.getCasualLeaves());
		leaveinfo.setEmpCode(employee.getEmpCode());
		leaveinfo.setSickLeaves(employee.getSickLeaves());
		leaveinfo.setTotalLeaves(employee.getTotalLeaves());
		return leaveinfo;
	}

	private PersonalInformation populatePersonalInfo(EmployeeInfoRequest employee) {
		PersonalInformation personalInfo=new PersonalInformation();
		personalInfo.setAge(employee.getAge());
		personalInfo.setDesignation(employee.getDesignation());
		personalInfo.setEmpCode(employee.getEmpCode());
		personalInfo.setEmpName(employee.getEmpName());
 		return personalInfo;
	}

	@Override
	public LeaveInformation retrieveEmployeInfo(String empCode) {
		int empId=getEmployeeIdFromCode(empCode);
		LeaveInformation leaveInfo=retrievEmployeeleaveInfo(empId);
		
		return leaveInfo;
	}

	private LeaveInformation retrievEmployeeleaveInfo(int empId) {
		String uri="http://localhost:8081/leaveInformation"+empId;
		LeaveInformation result = restTemplate.getForObject(uri, LeaveInformation.class);
		return result;

	}

	private int getEmployeeIdFromCode(String empCode) {
		String uri = "http://localhost:8080/personalInformation/code/"+empCode;
		PersonalInformation result = restTemplate.getForObject(uri, PersonalInformation.class);
		if(result!=null) {
		int empId =result.getId();
		return empId;
 	}
		return 0;
	}





}
