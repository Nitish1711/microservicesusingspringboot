/**
 * 
 */
package com.springBoot.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author Dell
 *
 */
@Configuration
public class EmployeeUtils {

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
