package com.springBoot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springBoot.model.EmployeeInfoRequest;
import com.springBoot.model.LeaveInformation;
import com.springBoot.service.EmployeeGatewayService;

@RestController
@Component
public class APIGatewayController {

	
	@Autowired
	private EmployeeGatewayService employeeGatewayService;

   //describing Aggregator pattern 	of micro service 
	@PostMapping(path="/employee",consumes = "application/json", produces = "application/json")
 	public String addEmployeeDetails(@RequestBody EmployeeInfoRequest employee) {
		System.out.println("saving employee info..");
		String result = "Failure";
		boolean isSaved = employeeGatewayService.saveEmployeeDetails(employee);
		if (isSaved) {
			result = "success";
		}
		return result;
	}
	
	
	//represents chain micro-service 
	@GetMapping("/employee/{empCode}")
	public LeaveInformation getEmployeeInfo(@PathVariable String empCode) {
		System.out.println("getEmployeeInfo");
	  return employeeGatewayService.retrieveEmployeInfo(empCode);
	}

	
 	
	/*
	 * @GetMapping("/personalInformation/{id}") public PersonalInformation
	 * getEmployeePersonalInfo(@PathVariable int id) {
	 * System.out.println("getting personal info"); return
	 * personalInfoService.getEmployeePersonalInfo(id); }
	 * 
	 * 
	 * @PutMapping(path="/personalInformation",consumes = "application/json",
	 * produces = "application/json")
	 * 
	 * public String editPersonalInfo(@RequestBody PersonalInformation
	 * employeeRequest) { System.out.println("editing personal info.."); String
	 * result = "faliure"; boolean isSaved =
	 * personalInfoService.saveEmployeePersonalInfo(employeeRequest); if (isSaved) {
	 * result = "success"; } return result; }
	 * 
	 * @DeleteMapping("/personalInformation/{id}") public boolean
	 * deleteEmployeePersonalInfo(@PathVariable int id) {
	 * System.out.println("deleting personal info");
	 * personalInfoService.removeEmployeePersonalInfo(id); return true; }
	 */

}
