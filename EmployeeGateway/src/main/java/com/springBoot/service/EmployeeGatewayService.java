package com.springBoot.service;

import com.springBoot.model.EmployeeInfoRequest;
import com.springBoot.model.EmployeeInfoResponse;
import com.springBoot.model.LeaveInformation;


public interface EmployeeGatewayService {
	
	

	public boolean saveEmployeeDetails(EmployeeInfoRequest employee);

	public LeaveInformation retrieveEmployeInfo(String empCode);
	
	/*
	 * public PersonalInformation getEmployeePersonalInfo(int id) ;
	 * 
	 * public void updateEmployeePersonalInfo(PersonalInformation personalInfo);
	 * 
	 * public boolean removeEmployeePersonalInfo(int id);
	 */

}
