package com.springBoot.model;

import java.util.Objects;

public class EmployeeInfoRequest  {

	
	private String empName;	
	
	private String age;
	private String empCode;
	
	private String designation;

	private Integer totalLeaves;
	private Integer casualLeaves;

	private Integer annualLeaves;
	private Integer sickLeaves;
	
	
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getEmpCode() {
		return empCode;
	}
	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public Integer getTotalLeaves() {
		return totalLeaves;
	}
	public void setTotalLeaves(Integer totalLeaves) {
		this.totalLeaves = totalLeaves;
	}
	public Integer getCasualLeaves() {
		return casualLeaves;
	}
	public void setCasualLeaves(Integer casualLeaves) {
		this.casualLeaves = casualLeaves;
	}
	public Integer getAnnualLeaves() {
		return annualLeaves;
	}
	public void setAnnualLeaves(Integer annualLeaves) {
		this.annualLeaves = annualLeaves;
	}
	public Integer getSickLeaves() {
		return sickLeaves;
	}
	public void setSickLeaves(Integer sickLeaves) {
		this.sickLeaves = sickLeaves;
	}
	@Override
	public int hashCode() {
		return Objects.hash(age, annualLeaves, casualLeaves, designation, empCode, empName, sickLeaves, totalLeaves);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeInfoRequest other = (EmployeeInfoRequest) obj;
		return Objects.equals(age, other.age) && Objects.equals(annualLeaves, other.annualLeaves)
				&& Objects.equals(casualLeaves, other.casualLeaves) && Objects.equals(designation, other.designation)
				&& Objects.equals(empCode, other.empCode) && Objects.equals(empName, other.empName)
				&& Objects.equals(sickLeaves, other.sickLeaves) && Objects.equals(totalLeaves, other.totalLeaves);
	}


}
