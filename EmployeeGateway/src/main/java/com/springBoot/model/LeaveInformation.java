/**
 * 
 */
package com.springBoot.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Dell
 *
 */

public class LeaveInformation {


	private Integer leaveId;
	
	private String empCode;

	private Integer totalLeaves;
	private Integer casualLeaves;

	private Integer annualLeaves;
	private Integer sickLeaves;
	

	public Integer getLeaveId() {
		return leaveId;
	}

	public void setLeaveId(Integer leaveId) {
		this.leaveId = leaveId;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public Integer getTotalLeaves() {
		return totalLeaves;
	}

	public void setTotalLeaves(Integer totalLeaves) {
		this.totalLeaves = totalLeaves;
	}

	public Integer getCasualLeaves() {
		return casualLeaves;
	}

	public void setCasualLeaves(Integer casualLeaves) {
		this.casualLeaves = casualLeaves;
	}

	public Integer getAnnualLeaves() {
		return annualLeaves;
	}

	public void setAnnualLeaves(Integer annualLeaves) {
		this.annualLeaves = annualLeaves;
	}

	public Integer getSickLeaves() {
		return sickLeaves;
	}

	public void setSickLeaves(Integer sickLeaves) {
		this.sickLeaves = sickLeaves;
	}

	@Override
	public int hashCode() {
		return Objects.hash(annualLeaves, casualLeaves, empCode, leaveId, sickLeaves, totalLeaves);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LeaveInformation other = (LeaveInformation) obj;
		return Objects.equals(annualLeaves, other.annualLeaves) && Objects.equals(casualLeaves, other.casualLeaves)
				&& Objects.equals(empCode, other.empCode) && Objects.equals(leaveId, other.leaveId)
				&& Objects.equals(sickLeaves, other.sickLeaves) && Objects.equals(totalLeaves, other.totalLeaves);
	}

}
