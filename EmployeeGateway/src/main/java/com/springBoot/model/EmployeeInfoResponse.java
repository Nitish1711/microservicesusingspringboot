package com.springBoot.model;

import java.io.Serializable;

public class EmployeeInfoResponse implements Serializable  {
 
	private static final long serialVersionUID = 1L;
	
	private String empName;	
 	private String empCode;
	
	private String designation;

	private Integer totalLeaves;

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpCode() {
		return empCode;
	}

	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Integer getTotalLeaves() {
		return totalLeaves;
	}

	public void setTotalLeaves(Integer totalLeaves) {
		this.totalLeaves = totalLeaves;
	}
 	
	
	 


}
