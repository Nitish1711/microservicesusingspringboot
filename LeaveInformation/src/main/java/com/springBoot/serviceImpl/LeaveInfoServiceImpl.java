package com.springBoot.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springBoot.entity.LeaveInformation;
import com.springBoot.repository.LeaveInfoRepository;
import com.springBoot.service.LeaveInfoService;

@Service
public class LeaveInfoServiceImpl implements LeaveInfoService {

	@Autowired
	private LeaveInfoRepository leaveInfoRepository;

	@Override
	public LeaveInformation saveEmployeeLeaveInfo(LeaveInformation leaveInfo) {
		LeaveInformation leaveInformtn=	leaveInfoRepository.save(leaveInfo);
		return leaveInformtn;

	}

	@Override
	public LeaveInformation getEmployeeLeaveInfo(int id) {
		Optional<LeaveInformation> perosnalInfo = leaveInfoRepository.findById(id);
		return perosnalInfo.get();
	}

	@Override
	public void updateEmployeeLeaveInfo(LeaveInformation personalInfo) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean removeEmployeeLeaveInfo(int id) {
		leaveInfoRepository.deleteById(id);
		return true;
	}

}
