package com.springBoot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.springBoot.entity.LeaveInformation;
import com.springBoot.service.LeaveInfoService;

@RestController
@Component
public class LeaveInformationController {

	@Autowired
	private LeaveInfoService leaveInfoService;

	@PostMapping(path = "/leaveInformation", consumes = "application/json", produces = "application/json")
	public ResponseEntity<LeaveInformation> addleaveInfo(@RequestBody LeaveInformation employeeRequest) {
		System.out.println("add leave info..");
		LeaveInformation isSaved = leaveInfoService.saveEmployeeLeaveInfo(employeeRequest);
	    return new ResponseEntity<LeaveInformation>(isSaved, HttpStatus.OK);
	}

	@GetMapping("/leaveInformation/{id}")
	public LeaveInformation getEmployeePersonalInfo(@PathVariable int id) {
		System.out.println("getting personal info");
		return leaveInfoService.getEmployeeLeaveInfo(id);
	}

	@PutMapping(path = "/personalInformation", consumes = "application/json", produces = "application/json")

	public String editPersonalInfo(@RequestBody LeaveInformation employeeRequest) {
		System.out.println("editing personal info..");
		String result = "faliure";
		LeaveInformation isSaved = leaveInfoService.saveEmployeeLeaveInfo(employeeRequest);
		/*
		 * if (isSaved) { result = "success";
		} */
		return result;
	}

	@DeleteMapping("/personalInformation/{id}")
	public boolean deleteEmployeePersonalInfo(@PathVariable int id) {
		System.out.println("deleting personal info");
		leaveInfoService.removeEmployeeLeaveInfo(id);
		return true;
	}

}
