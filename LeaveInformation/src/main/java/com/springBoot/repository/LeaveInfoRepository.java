package com.springBoot.repository;

import org.springframework.stereotype.Repository;

import com.springBoot.entity.LeaveInformation;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface LeaveInfoRepository extends JpaRepository<LeaveInformation,Integer> {
	
}
