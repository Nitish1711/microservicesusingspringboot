package com.springBoot.service;

import com.springBoot.entity.LeaveInformation;


public interface LeaveInfoService {
	
	

	public LeaveInformation saveEmployeeLeaveInfo(LeaveInformation leaveInfo);
	
	public LeaveInformation getEmployeeLeaveInfo(int id) ;
	
	public void updateEmployeeLeaveInfo(LeaveInformation leaveInfo);

	public boolean removeEmployeeLeaveInfo(int id);


}
